//
//  MenuVC.m
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "MenuVC.h"
#import "MenuCell.h"
#import "MenuModel.h"
#import "AuxDataManager.h"

@interface MenuVC () <UITableViewDelegate, UITableViewDataSource>
{
    NSArray *menuContent;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MenuVC

- (id) initWithMenuType:(enum MenuType)menuType andFrame:(CGRect)frame
{
    self = [super initWithNibName:@"MenuView" bundle:nil];
    
    if (self)
    {
        if (menuType == MenuTypeSell)
        {
            menuContent = [AuxDataManager loadJSONWithName:@"sellMenu"];
        }
        else if (menuType == MenuTypeBuy)
        {
            menuContent = [AuxDataManager loadJSONWithName:@"buyMenu"];
        }
        
        self.view.frame = frame;
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [self.tableView reloadData];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tableView setSeparatorColor:[UIColor grayColor]];
    
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return menuContent.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowsInSection = [[menuContent[section] valueForKey:@"content"] count];
    return rowsInSection;
}

#pragma mark - Table view delegate

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"menuCell";
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
 
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellReuseIdentifier:@"menuCell"];
        
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    
    return cell;
 }

- (void)tableView:(UITableView *)tableView willDisplayCell:(MenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuModel *menu = [self modelObjForIndexPath:indexPath];
    
    cell.lblTitleRow.text = [menu.content[indexPath.row] valueForKey:@"rowTitle"];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];

    [headerView setBackgroundColor:[UIColor lightGrayColor]];

    UILabel *headerTitle = [[UILabel alloc]initWithFrame:CGRectMake(25, 0, self.view.frame.size.width - 25, 50)];

    MenuModel *menuObj = [self modelObjForIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];

    NSString *title = menuObj.headerTitle;

    headerTitle.text = title;

    [headerView addSubview:headerTitle];

    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuModel *menu = [self modelObjForIndexPath:indexPath];
    
    UIStoryboard *st = [UIStoryboard storyboardWithName:[menu.content[indexPath.row] valueForKey:@"storyboard"] bundle:nil];
    
    UIViewController *vc = [st instantiateViewControllerWithIdentifier:[menu.content[indexPath.row] valueForKey:@"identifier"]];
    
//    [self.navigationController pushViewController:vc animated:YES];
    
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma PRIVATE METHOD

- (MenuModel *) modelObjForIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = menuContent[indexPath.section];
    
    NSError *error;
    
    MenuModel *menu = [MTLJSONAdapter modelOfClass:[MenuModel class] fromJSONDictionary:dic error:&error];
    
    if (!error)
    {
        return menu;
    }
    else
    {
        return nil;
    }

}


@end
