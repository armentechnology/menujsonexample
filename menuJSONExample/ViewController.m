//
//  ViewController.m
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "ViewController.h"
#import "MenuVC.h"

@interface ViewController ()
{
    MenuVC *menuObj;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ACTION BUTTONS

- (IBAction)btnShowMenu:(id)sender
{
    menuObj = nil;
    
    menuObj = [[MenuVC alloc]initWithMenuType:MenuTypeSell andFrame:self.view.frame];
    
    [self.view addSubview:menuObj.view];
}


@end
