//
//  MenuModel.h
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MenuContent : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *sort;
@property (nonatomic, copy, readonly) NSString *rowTitle;
@property (nonatomic, copy, readonly) NSString *storyboard;
@property (nonatomic, copy, readonly) NSString *identifier;

@end

@interface MenuModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *sort;
@property (nonatomic, copy, readonly) NSString *headerTitle;
@property (nonatomic, copy, readonly) NSArray <MenuContent *> *content;

@end

