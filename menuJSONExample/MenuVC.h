//
//  MenuVC.h
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <UIKit/UIKit.h>

 enum MenuType
{
    MenuTypeSell,
    MenuTypeBuy
};

@interface MenuVC : UIViewController

- (id) initWithMenuType:(enum MenuType)menuType andFrame:(CGRect)frame;

@end
