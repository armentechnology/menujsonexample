//
//  MenuCell.m
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
