//
//  AuxDataManager.h
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuxDataManager : NSObject

+ (NSArray *)loadJSONWithName:(NSString *)jsonName;

@end
