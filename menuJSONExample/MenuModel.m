//
//  MenuModel.m
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "MenuModel.h"

@implementation MenuContent

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"sort" : @"sort",
             @"rowTitle" : @"rowTitle",
             @"storyboard" : @"storyboard",
             @"identifier" : @"identifier"
             };
}

@end

@implementation MenuModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"sort" : @"sort",
             @"headerTitle" : @"headerTitle",
             @"content" : @"content"
             };
}

+ (NSValueTransformer *)topupHistoryRecordsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[MenuContent class]];
}

/*
 * Deserialize with
 *
 * MenuModel *menu = [MTLJSONAdapter modelOfClass:[MenuModel class] fromJSONDictionary:JSONDictionary error:&error];
 *
 */



@end

