//
//  MenuCell.h
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitleRow;

@end
