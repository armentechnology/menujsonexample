//
//  AuxDataManager.m
//  menuJSONExample
//
//  Created by Jose Catala on 19/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "AuxDataManager.h"

@implementation AuxDataManager


+ (NSArray *)loadJSONWithName:(NSString *)jsonName
{
    NSString * filePath =[[NSBundle mainBundle] pathForResource:jsonName ofType:@"json"];
    
    NSError * error;
    
    NSString* fileContents =[NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    
    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
        
        return @[];
    }
    
    NSArray *returnArray = (NSArray *)[NSJSONSerialization
                                       JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                                       options:0 error:&error];
    
    if (error)
    {
        NSLog(@"Error parsing JSON: %@",error.localizedDescription);
        
        return @[];
    }
    
    NSSortDescriptor *firstDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:firstDescriptor, nil];
    
    NSArray *sortedArray = [returnArray sortedArrayUsingDescriptors:sortDescriptors];
    
    return sortedArray;
}

@end
